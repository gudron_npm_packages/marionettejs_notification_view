(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("backbone.marionette"), require("noty"));
	else if(typeof define === 'function' && define.amd)
		define(["backbone.marionette", "noty"], factory);
	else {
		var a = typeof exports === 'object' ? factory(require("backbone.marionette"), require("noty")) : factory(root["backbone.marionette"], root["noty"]);
		for(var i in a) (typeof exports === 'object' ? exports : root)[i] = a[i];
	}
})(this, function(__WEBPACK_EXTERNAL_MODULE_0__, __WEBPACK_EXTERNAL_MODULE_1__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 2);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = require("backbone.marionette");

/***/ }),
/* 1 */
/***/ (function(module, exports) {

module.exports = require("noty");

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _version = __webpack_require__(3);

var _backbone = __webpack_require__(0);

var _backbone2 = _interopRequireDefault(_backbone);

var _notificationItemView = __webpack_require__(4);

var _notificationItemView2 = _interopRequireDefault(_notificationItemView);

var _noty = __webpack_require__(1);

var _noty2 = _interopRequireDefault(_noty);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// NotificationLayoutView
// ---------

var NotificationLayoutView = _backbone2.default.CollectionView.extend({
    version: _version.version,
    options: {
        theme: 'mint',
        layout: 'topCenter'
    },
    className: 'noty-container',
    template: false,
    childView: _notificationItemView2.default,
    _notificationNumber: 0,
    childEvents: {
        'noty:close': function notyClose(view) {
            this.removeChildView(view);
        }
    },
    initialize: function initialize() {
        var that = this;

        _noty2.default.overrideDefaults(_.extend({
            maxVisible: 5,
            killer: false,
            timeout: 5000,
            progressBar: false
        }, this.options));
    },
    addChild: function addChild(child, ChildView) {
        var args = [child, ChildView, this._notificationNumber++];
        _backbone2.default.CollectionView.prototype.addChild.apply(this, args);
    }
});

NotificationLayoutView.itemView = _notificationItemView2.default;

exports.default = NotificationLayoutView;

/***/ }),
/* 3 */
/***/ (function(module, exports) {

module.exports = {"version":"0.0.5","buildDate":"2017-12-15"}

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _backbone = __webpack_require__(0);

var _backbone2 = _interopRequireDefault(_backbone);

var _noty = __webpack_require__(1);

var _noty2 = _interopRequireDefault(_noty);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// NotificationItemView
// ---------

var NotificationItemView = _backbone2.default.ItemView.extend({
    className: 'noty-box',
    template: false,
    noty: false,
    id: function id() {
        return 'noty_' + Date.now();
    },
    initialize: function initialize() {
        var that = this;
    },
    onAttach: function onAttach() {
        var that = this;

        this.noty = new _noty2.default({
            text: this.model.text,
            type: this.model.type,
            container: '#' + this.$el.attr('id'),
            callbacks: {
                afterClose: function afterClose() {
                    that.triggerMethod('noty:close', that);
                },
                afterShow: function afterShow() {
                    if (that.model.callback && _.isFunction(that.model.callback)) {
                        that.model.callback.apply();
                    }
                }
            }
        });

        this.noty.show();
    },
    onBeforeDestroy: function onBeforeDestroy() {
        delete this.noty;
        this.noty = false;
    }
});

exports.default = NotificationItemView;

/***/ })
/******/ ]);
});
//# sourceMappingURL=notification.view.js.map