# Changelog

## [Unreleased]
- Migration to Marionettejs v3

## [v0.0.5] - 2017-12-13
### Fixed
- extended of default Noty options

## [v0.0.4] - 2017-12-13
### Changed
- main script @ package.json

## [v0.0.3] - 2017-12-13
### Added
- Ansible build in docker container playbook

### Changed
- Plugin exporting

## [v0.0.2] - 2017-11-10
### Added
- Semantic versioning

## [v0.0.1] - 2017-11-05
### Added
- Plugin initial  